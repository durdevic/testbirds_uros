import React, { Component } from "react";
import "./App.scss";
import Autosuggest from "react-autosuggest";
import data from "./assets/data.json";
import InnerSelectionCard from "./components/InnerSelectionCard/InnerSelectionCard";
import PersonComponent from "./components/PersonComponent/PersonComponent";
import Group from "./assets/group.svg";

class App extends Component {
  title = "your team for this test";
  constructor() {
    super();
    // Autosuggest is a controlled component.
    // This means that you need to provide an input value
    // and an onChange handler that updates this value (see below).
    // Suggestions also need to be provided to the Autosuggest,
    // and they are initially empty because the Autosuggest is closed.
    this.state = {
      suggestedPeople: [],
      value: "",
      suggestions: [],
      selectedItems: [],
      showAutoSuggest: false
    };
  }

  componentDidMount() {
    this.setState({
      suggestedPeople: data
    });
  }

  // Teach Autosuggest how to calculate suggestions for any given input value.
  getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0
      ? []
      : this.state.suggestedPeople.filter(
          lang =>
            lang.username.toLowerCase().slice(0, inputLength) === inputValue
        );
  };

  // When suggestion is clicked, Autosuggest needs to populate the input
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  getSuggestionValue = suggestion => suggestion.username;

  renderSuggestion = suggestion => {
    // here should render the empty section but it's not rendering after
    return <InnerSelectionCard suggestion={suggestion} />;
    // return (
    // <div style={{justifyContent: 'center',alignItems: 'center', textAlign: 'center'}}>
    //       <div style={{color: '#007672'}}>Team member not found.</div>
    //       <div>Maybe she/he is not yet in your <span style={{textDecoration: 'underline'}}>team?</span> </div>
    //     </div>
    //     )
  };

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    console.log("this");
    console.log(value);

    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  onSuggestionSelected = (event, { suggestion }) => {
    var newList = this.state.selectedItems;
    newList.push(suggestion);
    let newSuggestions = this.state.suggestedPeople;
    let index = newSuggestions.findIndex(
      listElement => listElement.id === suggestion.id
    );

    newSuggestions.splice(index, 1);

    this.setState({
      selectedItems: newList,
      suggestedPeople: newSuggestions,
      value: ""
    });
  };

  renderListItems = () => {
    let listItems;
    if (this.state.selectedItems.length < 5) {
      listItems = this.state.selectedItems.map((item, index) => (
        <PersonComponent
          key={index}
          person={item}
          onRemove={id => this.removeItem(id)}
        />
      ));
    } else {
      listItems = this.state.selectedItems.map((item, index) => {
        if (index < 5) {
          return (
            <PersonComponent
              key={index}
              person={item}
              onRemove={id => this.removeItem(id)}
            />
          );
        }
      });
    }

    return listItems;
  };

  removeItem = id => {
    //removing from suggested
    let currentList = this.state.selectedItems;
    let index = currentList.findIndex(listElement => listElement.id === id);

    // adding back deleted
    let previousList = [...this.state.suggestedPeople];
    var deleted = currentList.splice(index, 1);
    previousList.push(deleted[0]);

    this.setState({
      selectedItems: currentList,
      suggestedPeople: previousList
    });
  };

  renderButtonOrAutoSuggest = () => {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: "Find a member",
      value,
      onChange: this.onChange
    };
    if (this.state.showAutoSuggest) {
      return (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            paddingTop: 10,
            paddingBottom: 10
          }}
        >
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={this.getSuggestionValue}
            renderSuggestion={this.renderSuggestion}
            inputProps={inputProps}
            onSuggestionSelected={this.onSuggestionSelected}
          />
          <div />
          <div
            style={{ padding: 10, zIndex: 3, cursor: "pointer" }}
            onClick={() => this.setState({ showAutoSuggest: false })}
          >
            <i
              className={`fas fa-times`}
              style={{ fontSize: 22, color: "orange" }}
            />
          </div>
        </div>
      );
    } else {
      return (
        // <div
        //   onClick={() => this.setState({ showAutoSuggest: true })}
        //   style={{ height: 50, width: 50 }}
        // >
        //   button
        // </div>

        <div
          className="wrapper-add"
          onClick={() => this.setState({ showAutoSuggest: true })}
        >
          <div className={`plus-button`}>
            <span style={{ marginTop: -5 }}>+</span>
          </div>
          <div>
            <div style={{ fontWeight: "600", color: "#007672" }}>
              Add team member to this test
            </div>
          </div>
        </div>
      );
    }
  };

  renderMoreButton = () => {
    if (this.state.selectedItems.length > 5) {
      return (
        <div className="more-button" style={{}}>
          <div style={{ color: "#007672" }}>Show All</div>
        </div>
      );
    } else {
      return null;
    }
  };

  render() {
    return (
      <div className="GeneralContainer">
        <div
          style={{ display: "flex", flexDirection: "row", marginBottom: 16 }}
        >
          <div className="title">{this.title}</div>
          <div style={{ flex: 1 }} />
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              alignItems: "center"
            }}
          >
            <div className="teamPageText" style={{ fontSize: 12 }}>
              TEAM PAGE
            </div>
            <img
              src={Group}
              alt=""
              style={{ width: 20, height: 20, marginLeft: 10 }}
            />
          </div>
        </div>

        <div className="Container">
          {this.renderButtonOrAutoSuggest()}

          {this.renderListItems()}
        </div>
        {this.renderMoreButton()}
      </div>
    );
  }
}

export default App;
