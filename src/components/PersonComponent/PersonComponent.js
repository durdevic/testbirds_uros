import React, { Component } from "react";
import AvatarPhoto from "../../assets/avatar-default.png";
import CancelButton from "../../assets/cancel.svg"
import "./PersonComponent.scss";

export class PersonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCancel: false
    };
  }

  hoverAvatar = () => {
    console.log('show')
    this.setState({
      showCancel: true
    });
  };

  leaveAvatar = () => {
    console.log('hide')
    this.setState({
      showCancel: false
    });
  };

  handleRemove = personId => {
    this.props.onRemove(personId);
  };

  renderAvatarImage = () => {
    if (!this.state.showCancel) {
      return <img src={AvatarPhoto} className="avatar-image" alt="avatar" />;
    } else {
      return (
        <div className={`cancel-button`}>
          <img src={CancelButton} alt="cancel" style={{height: 15, width: 15}}/>
        </div>
      );
    }
  };

  render() {
    return (
      <div
        className="wrapper"
        onMouseEnter={this.hoverAvatar}
        onMouseLeave={this.leaveAvatar}
      >
        <div
          className="avatar"
          onClick={() => this.handleRemove(this.props.person.id)}
        >
          {this.renderAvatarImage()}
        </div>

        <div style={{ display: "flex", flexDirection: "row" }}>
          <div>
            <div>{this.props.person.role}</div>
            <div style={{ fontWeight: "600" }}>
              {this.props.person.username}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PersonComponent;
