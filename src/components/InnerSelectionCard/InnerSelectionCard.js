import React from "react";
import AvatarPhoto from "../../assets/avatar-default.png";
import "./InnerSelectionCard.scss";

function InnerSelectionCard(props) {
  return (
    <div className="wrapper-inner-card">
      <div className="avatar">
        <img
          src={AvatarPhoto}
          className="avatar-image"
          alt="avatar"
          style={{ marginLeft: 10, marginRight: 10 }}
        />
      </div>
      <div>
        <div>{props.suggestion.username}</div>
      </div>
    </div>
  );
}

export default InnerSelectionCard;
